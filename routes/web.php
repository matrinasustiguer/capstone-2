<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/Meet-Our-Friends', 'PetController@index');

Route::middleware("customer")->group(function(){
    Route::post('/add-to-book/{pet_id}', 'BookController@addToBook');
    Route::get('/book', 'BookController@index');
    Route::get('/remove-from-book/{petId}', 'BookController@removeFromBook');
    Route::get('/empty-book', 'BookController@destroy');
    
    Route::post('/book-visit', 'VisitController@bookVisit');
    Route::get('/my-visits', 'VisitController@show');
});

Route::middleware("frontdesk")->group(function(){

    Route::get('/add-pet-form', 'PetController@create');
    Route::post('/add-pet-form', 'PetController@store');
    Route::delete('/delete-pet', 'PetController@destroy');
    Route::get('/update-pet/{id}', 'PetController@edit');
    Route::patch('/update-pet/{id}', 'PetController@update');

    Route::get('/all-bookings', 'OrderController@index');
});

Route::middleware("admin")->group(function(){
    
    // Route::get('/payments', 'Controller@index');
    // Route::post('/add-payment', 'Controller@store');
    // Route::delete('/delete-payment', 'Controller@destroy');

    // Route::get('/statuses', 'StatusController@index');
    // Route::post('/add-status', 'StatusController@store');
    // Route::delete('/delete-status', 'StatusController@destroy');

    Route::get('/types', 'TypeController@index');
    Route::post('/add-type', 'TypeController@store');
    Route::delete('/delete-type', 'TypeController@destroy');

    // Route::get('/add-pet-form', 'PetController@create');
    Route::post('/add-pet-form', 'PetController@store');
    Route::delete('/delete-pet', 'PetController@destroy');
    Route::get('/update-pet/{id}', 'PetController@edit');
    Route::patch('/update-pet/{id}', 'PetController@update');

    Route::get('/all-visit-bookings', 'VisitController@index');
});

Route::get('/my-profile', 'ProfileController@show');
Route::get('/all-profiles', 'ProfileController@index');
Route::post('/add-profile', 'ProfileController@store');
Route::delete('/delete-profile', 'ProfileController@destroy');
Route::get('/update-profile/{id}', 'ProfileController@edit');
Route::patch('/update-profile/{id}', 'ProfileController@update');
