<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    public function pets(){
        return $this->belongsToMany('App\Pet')->withTimeStamps();
    }

    public function standing(){
        return $this->belongsTo('App\Standing');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
