<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    public function type(){ 
        return $this->belongsTo('App\Type');
    }

    public function status(){ 
        return $this->belongsTo('App\Status');
    }
}
