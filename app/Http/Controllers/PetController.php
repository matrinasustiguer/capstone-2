<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pet;
use App\Status;
use App\Type;
use Session;

class PetController extends Controller
{
    public function index(Request $request){

        $pets = Pet::all();
        $statuses = Status::all();
        $types = Type::all();

        if($request->status_id){
            $pets = Pet::where('status_id', $request->status_id)->get();
        }

        if($request->type_id){
            $pets = Pet::where('type_id', $request->type_id)->get();
        }

        if($request->sort_asc){
            $pets = $pets->sortBy('age');
            $pets->values()->all();
        }

        if($request->sort_desc){
            $pets = $pets->sortByDesc('age');
            $pets->values()->all();
        }

        // dd(Session::all());
        return view('pets', compact('pets', 'statuses', 'types'));
    }

    // public function create(){
    //     $types = Type::all();
    //     return view('add-pet', compact ('types'));
    // }

    public function store(Request $request){

        $this->validate($request, [
            "name" => 'required',
            "type_id" => 'required',
            "description" => 'required',
            "imgPath" => 'required',
        ]);
        
        $new_pet = new Pet;
        $new_pet->name = $request->name;
        $new_pet->type_id = $request->type_id;
        $new_pet->description = $request->description;
        $new_pet->age = $request->age;
        $new_pet->status_id = 1;
        
        $image = $request->file('imgPath');
        $image_name = time() . "." . $image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $image_name);
        $result = \Cloudinary\Uploader::upload($destination.$image_name);
        
        $new_pet->imgPath = $result['secure_url'];
        $new_pet->save();

        return redirect('/Meet-Our-Friends');
    }

    public function destroy(Request $request){
        $id = $request->pet_id;
        $pet = Pet::find($id);
        $pet->delete();
        return redirect('/Meet-Our-Friends');
    }

    public function edit($id){

        $pet = Pet::find($id);
        $types = Type::all();

        return view('edit-pet', compact('pet', 'types'));
    }

    public function update($id, Request $request){
        
        $pet = Pet::find($id);
        $pet->name = $request->name;
        $pet->type_id = $request->type_id;
        $pet->description = $request->description;
        $pet->age = $request->age;
        $pet->status_id = $request->status_id;

        $image_result = $pet->imgPath;

        if($request->file('imgPath') == ""){
            $pet->imgPath = $image_result;
        }else{
            $image = $request->file('imgPath');
            $image_name = time() . "." . $image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $pet->imgPath = $destination . $image_name;
        }
        
        $pet->save();

        return redirect ('/Meet-Our-Friends');
    }
}
