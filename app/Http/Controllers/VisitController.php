<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Visit;
use App\Pet;


class VisitController extends Controller
{
    public function bookVisit(Request $request){
        

        $visit = new Visit;
        $visit->schedule = $request->schedule;
        $visit->notes = $request->notes;
        $visit->standing_id = Auth::user()->standing_id;
        $visit->user_id = Auth::user()->id;

        $visit->save();

        $book = Session::get('book');

        foreach($book as $pet_id => $pet_name){

            $visit->pets()->attach($pet_id);

            $pet = Pet::find($pet_id);
            $pet->save();

        }

        Session::forget('book');
        return redirect ('/my-visits');
    }

    public function show(){
        $visits = Visit::where("user_id", Auth::user()->id)->get();
        return view('user-visit', compact ('visits'));
    }

    public function index(){
        
        $visits = Visit::all();
        return view('visits', compact ('visits'));
    }
}
