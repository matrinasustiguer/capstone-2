<?php

namespace App\Http\Controllers;

use App\Profile;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $profile = Profile::where('user_id', $id);

        return view('profile', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "contact" => 'required',
            "address" => 'required',
            "about" => 'required',
            "profImg" => 'required',
        ]);
        
        $new_profile = new Profile;
        $new_profile->contact = $request->contact;
        $new_profile->address = $request->address;
        $new_profile->about = $request->about;
        $new_profile->user_id = Auth::user()->id;
        
        $image = $request->file('profImg');
        $image_name = time() . "." . $image->getClientOriginalExtension();
        $destination = "images/profiles/";
        $image->move($destination, $image_name);
        $result = \Cloudinary\Uploader::upload($destination.$image_name);
        
        $new_profile->profImg = $result['secure_url'];
        $new_profile->save();

        return redirect('/my-profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        $profile = Profile::where('user_id', Auth::user()->id)->first();

        return view('profile', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id);
        $user = Auth::user();

        return view('edit-profile', compact('profile', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $profile = Profile::find($id);
        $profile->contact = $request->contact;
        $profile->address = $request->address;
        $profile->about = $request->about;
        $profile->user_id = $profile->user_id;

        $image_result = $profile->profImg;

        if($request->file('profImg') == ""){
            $profile->profImg = $image_result;
        }else{
            $image = $request->file('profImg');
            $image_name = time() . "." . $image->getClientOriginalExtension();
            $destination = "images/profiles/";
            $image->move($destination, $image_name);
            $profile->profImg = $destination . $image_name;
            $result = \Cloudinary\Uploader::upload($destination.$image_name);
        
            $profile->profImg = $result['secure_url'];
        }
        
        $profile->save();

        return redirect ('/my-profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->profile_id;
        $profile = Profile::find($id);
        $profile->delete();
        return redirect('/my-profile');
    }
}
