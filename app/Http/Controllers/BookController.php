<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pet;
use Session;

class BookController extends Controller
{
    public function addToBook($pet_id, Request $request){
        $pet = Pet::find($pet_id);

        if(Session::has('book')){
            $book = Session::get('book');
        }else{
            $cart = [];
        }

            $book[$pet_id] = $request->pet_name;
            
        Session::put('book', $book);

        return "Success";
    }

    public function index(){
        $pet_book = [];

        if (Session::has('book')){
            $book = Session::get('book');
            foreach($book as $pet_id=>$pet_name){
                $pet = Pet::find($pet_id);

                $pet_book[] = $pet;
            }
        }
        
        return view('book', compact('pet_book'));
    }

    public function removeFromBook($petId){
        Session::forget("book.$petId");
        return back();
    }

    public function destroy(){
        Session::forget('book');
        return back();
    }

}
