<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;

class TypeController extends Controller
{
    public function index(){

        $types = Type::all();

        return view('types-page', compact('types'));
    }

    public function store(Request $request){
        $new_type = new Type;
        $new_type->name = $request->name;
        $new_type->save();

        return redirect('types');
    }

    public function destroy(Request $request){
        $id = $request->category_id;
        $type = Type::find($id);
        $type->delete();
        return redirect('types');
    }
}
