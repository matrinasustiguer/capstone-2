@extends('layouts.template')
@section('title', 'Pet Type Forms')
@section('content')
    <h1 class="text-center py-5">Pet Types</h1>
    <div class="text-center py-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 offset-lg-4">
                    <form action="add-type" method="POST" class="alpha-form">
                    @csrf
                    <div class="form-group">
                        <label for="name">Type Name:</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-warning">Add Type</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container alpha-table">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Type ID</th>
                            <th>Type Name</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($types as $type)
                        <tr>
                            <td>{{$type->id}}</td>
                            <td>{{$type->name}}</td>
                            <td class="d-flex">
                                <form action="/delete-type" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="hidden" name="type_id" value="{{$type->id}}">
                                    <button type="submit" class="btn btn-danger">DELETE</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection