@extends('layouts.ntemplate')
@section('title', "Prester's | Visit Pet")
@section('content')
        <div class="container">
                <div class="d-flex justify-content-center align-items-center flex-column col-lg-10 offset-lg-1">
                    <h1 class="m-2 p-2 font-italic">Book Your Visit</h1>
                    <!-- Button trigger modal visit-->
                    <div class="text-center p-2 clear-both">
                        <button type="button" class="btn-lg btn-info" data-toggle="modal" data-target="#bookModal">Book Now</button>
                    </div>


                    <!-- Modal Book-->
                    <div class="vh-100 modal fade" id="bookModal" tabindex="-1" role="dialog" aria-labelledby="bookModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="bookModalLabel">Booking</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <form action="/book-visit" method="POST" class="alpha-form" data-remote="true">
                                            @csrf
                                            @include('layouts.errors')
                                                <div class="form-group">
                                                    <label for="schedule">Date and Time of Visit:</label>
                                                    <input type="datetime-local" name="schedule" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="notes">Notes:</label>
                                                    <textarea name="notes" class="form-control" rows="10" cols="50" placeholder="Tell us something you want us to know. Example: Are you visiting with someone (person or pet)?"></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-warning">Book</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="p-3 alpha-form m-5">
                        <h4 class="m-2">Pet(s) I'm Going To Visit:</h4>
                        <div class="row">
                        @foreach($pet_book as $pet)
                        <div class="col-lg-4 py-1">
                            <div class="card m-1 rounded-circle">
                                <img class="card-img-top rounded-circle img-thumbnail" src="{{$pet->imgPath}}">
                                <div class="card-body text-center">
                                    <h3 class="card-title">{{$pet->name}}</h3>
                                    <p class="card-text">Age: {{$pet->age}} yrs. old</p>
                                    <p class="card-text">Description: {{$pet->description}}</p>
                                    <p class="card-text">Type: {{$pet->type->name}}</p>
                                </div>
            
                                <div class="card-footer d-flex justify-content-center">
                                    <a href="/remove-from-book/{{$pet->id}}" class="btn btn-danger">Remove From Visit</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div> 

@endsection