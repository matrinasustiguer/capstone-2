@extends('layouts.template')
@section('title', 'Edit Pet Form')
@section('content')
    <h1 class="text-center py-5">Edit Pet Details</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/update-pet/{{$pet->id}}" method="POST" enctype="multipart/form-data" class="alpha-form">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="imgPath">Pet Image:</label>
                    <img src="{{ url($pet->imgPath) }}" height="50px" width="50px" alt="">
                    <input type="file" name="imgPath" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="name">Pet Name:</label>
                    <input type="text" name="name" class="form-control" value="{{$pet->name}}">
                </div>
                <div class="form-group">
                    <label for="age">Pet Age:</label>
                    <input type="number" name="age" class="form-control" value="{{$pet->age}}">
                </div>
                <div class="form-group">
                    <label for="type_id">Type:</label>
                    <select name="type_id" class="form-control">
                        @foreach($types as $type)
                        <option value="{{$type->id}}" {{ ( $pet->type_id == $type->id) ? 'selected' : '' }} >{{$type->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="description">Pet Description:</label>
                    <textarea name="description" class="form-control">{{$pet->description}}</textarea>
                </div>
                
                <input type="hidden" name="status_id" value="{{$pet->status_id}}">
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection