@extends('layouts.ntemplate')
@section('title', 'All Booked Visits')
@section('content')
<h1 class="text-center py-3">All Bookings</h1>
<div class="container">
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <table class="table table-striped bg-white">
                <thead>
                    <tr>
                        <th>Visit ID</th>
                        <th>Booking Date</th>
                        <th>User Details</th>
                        <th>Pets to Visit</th>
                        <th>Schedule / Booked Date</th>
                        <th>Notes</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($visits as $visit)
                    <tr>
                        <td>{{$visit->created_at->format('U')}}</td>
                        <td>{{$visit->created_at->diffForHumans()}}</td>
                        <td>{{$visit->user->name}}</td>
                        <td>
                            @foreach($visit->pets as $pet)
                                {{ $pet->name }}, 
                            @endforeach
                        </td>
                        <td>{{$visit->schedule}}</td>
                        <td>{{$visit->notes}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection