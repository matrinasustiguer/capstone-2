<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Prester's</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://bootswatch.com/4/litera/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/welcome.css') }}">
    </head>
    <body>
        @if (Route::has('login'))
            <div class="upper-right">
                @auth
                    <a href="{{ url('/home') }}" class="btn btn-warning">Home</a>
                @else
                    <a href="{{ route('login') }}" class="btn btn-outline-warning">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="btn btn-outline-warning">Register</a>
                    @endif
                @endauth
            </div>
        @endif
        <div class="text-warning"></div>
        <div class="col-lg-6 float-left font-weight-bold font-italic text-center">
            <img src="https://res.cloudinary.com/djo1ucwnk/image/upload/v1598419361/efpazr1kjtkkdvhh1ffo.png" alt="" class="prester-big rounded-circle">
            <div class="prester-name">Prester's Rescue and Shelter Society</div>
            <a href="{{ url('/Meet-Our-Friends') }}" class="btn visit-us-now">Visit Us</a>  
        </div>
        <div class="d-none d-lg-block float-left col-lg-6">
            <div class="card rounded about-us landing-text">
                <div class="card-body">
                    <h3 class="card-title p-1">About Us</h3>
                    <p class="card-text">Prester's Pet Rescue and Shelter Society</p>
                    <p class="card-text">We are dedicated to rescuing animals and offering them shelter if not their next home. We now accept adoption applications from prospective parents interested. Here is our general adoption process</p>
                    <p class="card-text">First: Let us get to know you. Login. (Not yet registered? Register.</p>
                    <p class="card-text">Second: On your right, you will see our pet friends who are up for adoption. Click Visit Me to schedule meeting with them. You can schedule more than one pet per visit.</p>
                    <p class="card-text">Third: After three successful visits, we will conduct a short interview and proceed with the paperworks. We have complete adoption papers and medical records - that's why we do have a small ADOPTION FEE. We keep them up to date. We love our pet friends.</p>
                    <p class="card-text">Fourth: You may bring your lovely pet home after the signing of papers.</p>
                    <p class="card-text">Fifth: We will keep in touch with you and visit our friend in your house after 2 days.</p>
                    <p class="card-text">We reserve the right to visit our pet friends at least twice a year with your permission in accordance with our policy. Again, we love our pet friends and it is in our intention that both of you are compatible and happy!</p>
                    <p class="card-text pt-1 mt-5 mx-auto align-items-center font-weight-bold">Contact Details:</p>
                    <p class="card-text mx-auto">
                        Call us: +02 7550 2X8X<br>
                        Phone Number: +63 906 701 4X1X<br>
                        Email: frontdesk@presters.com
                        Find us near SM Bicutan, Dona Soledad Avenue, Paranaque, 1709 Metro Manila
                    </p>
                    <div class="d-block m-auto p-2 text-center align-items-center">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3862.9609282421916!2d121.04195991459638!3d14.486932489874944!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397cf051470efb5%3A0xc5698f639aacfe68!2sSM%20City%20Bicutan!5e0!3m2!1sen!2sph!4v1591557320236!5m2!1sen!2sph" width="90%" height="80%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
