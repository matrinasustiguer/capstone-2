@extends('layouts.template')
@section('title', 'Edit Profile')
@section('content')
    <h1 class="text-center py-5">Edit Profile</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/update-profile/{{$profile->id}}" method="POST" enctype="multipart/form-data" class="alpha-form">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="imgPath">Profile Picture:</label>
                    <img src="{{ url($profile->profImg) }}" height="50px" width="50px" alt="" class="m-1">
                    <input type="file" name="profImg" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="contact">Contact Number:</label>
                    <input type="text" name="contact" class="form-control" value="{{$profile->contact}}">
                </div>
                <div class="form-group">
                    <label for="address">Address:</label>
                    <input type="text" name="address" class="form-control" value="{{$profile->address}}">
                </div>
                <div class="form-group">
                    <label for="about">About Me:</label>
                    <textarea name="about" class="form-control">{{$profile->about}}</textarea>
                </div>
                
                <input type="hidden" name="status_id" value="{{$profile->status_id}}">
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection