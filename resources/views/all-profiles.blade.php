@extends('layouts.template')
@section('title', "Prester's Friends")
@section('content')

@if(Session::has("message"))
    <h4 class="text-danger text-center">{{Session::get('message')}}</h4>
@endif
<!-- @dump(Session::get('book')) -->
    <div class="container">
        <div class="row">
            <div class="col-lg-3 vh-90">
                
            </div>
            <div class="col-lg-9">
                <h1 class="text-center py-3">Meet Our Friends</h1>
                @auth
                    @if(Auth::user()->role_id === 2 | Auth::user()->role_id === 3)
                        <!-- Button trigger modal add-->
                        <div class="text-center p-2 clear-both">
                            <button type="button" class="btn-lg btn-outline-warning" data-toggle="modal" data-target="#exampleModal">Add Pet</button>
                        </div>
                        <!-- Modal Add-->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Pet Form</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <!-- <div class="col-lg-4 offset-lg-4"> -->
                                            <form action="/add-pet-form" method="POST" enctype="multipart/form-data" class="alpha-form" data-remote="true">
                                            @csrf
                                            @include('layouts.errors')
                                                <div class="form-group">
                                                    <label for="imgPath">Pet Image:</label>
                                                    <input type="file" name="imgPath" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name">Pet Name:</label>
                                                    <input type="text" name="name" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="age">Pet Age:</label>
                                                    <input type="number" name="age" class="form-control" value="{{$pet->name}}>
                                                </div>
                                                <div class="form-group">
                                                    <label for="type_id">Type:</label>
                                                    <select name="type_id" class="form-control">
                                                        @foreach($types as $type)
                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="description">Pet Description:</label>
                                                    <textarea name="description" class="form-control"></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-warning">Add Pet</button>
                                            </form>
                                        <!-- </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    @endif
                @endauth
                
                <div>
                    <h4>Filter by Type</h4>
                    <div class="list-group border list-group-horizontal my-1">
                        <a href="/Meet-Our-Friends" class="list-group-item list-group-item-action flex-fill text-center">All</a>
                        @foreach($types as $type)
                        <a href="/Meet-Our-Friends?type_id={{$type->id}}" class="list-group-item list-group-item-action flex-fill text-center">{{$type->name}}</a>
                        @endforeach
                    </div>
                </div>

                <div class="row">      
                    @foreach($pets as $pet)
                    <div class="col-lg-4 py-1">
                        <div class="card m-1 rounded-circle">
                            <img class="card-img-top rounded-circle img-thumbnail" src="{{$pet->imgPath}}">
                            <div class="card-body text-center">
                                <h3 class="card-title">{{$pet->name}}</h3>
                                <p class="card-text">Age: {{$pet->age}} yrs. old</p>
                                <p class="card-text">Description: {{$pet->description}}</p>
                                <p class="card-text">Type: {{$pet->type->name}}</p>
                            </div>
                            
                            @auth
                                @if(Auth::user()->role_id === 2 | Auth::user()->role_id === 3)

                            <div class="card-footer d-flex justify-content-center">
                                <form action="/delete-pet" method="POST">
                                @csrf
                                @method('DELETE')
                                    <input type="hidden" name="pet_id" value="{{$pet->id}}">
                                    <button type="submit" class="btn btn-danger mx-1">DELETE</button>
                                </form>
                                    <a href="/update-pet/{{$pet->id}}" class="btn btn-warning">Update</a>
                            </div>

                                @else
                                
                            <div class="card-footer d-flex justify-content-center">
                                <button class="btn btn-primary addToBookBtn">Visit Me</button>                                        
                            </div>
                            @endif
                            @endauth
                        </div>
                    </div>
                    @endforeach
                </div>
                
            </div> 
        </div>
    </div>

    <script type="text/javascript">
        const addToBookBtns = document.querySelectorAll('.addToBookBtn');
        addToBookBtns.forEach( function (addToBookBtn){
            addToBookBtn.addEventListener('click', function(e){
            
                const book_this = e.target.parentElement.previousElementSibling.value;
                const pet_id = e.target.parentElement.previousElementSibling.getAttribute('data-id');

                let data = new FormData;
                data.append("_token", "{{ csrf_token() }}");
                data.append("book_this", book_this);

                fetch("/add-to-book/" + pet_id, {
                    method: "POST",
                    body: data //we need the data from the form
                }).then(function (response){
                    return response.text();
                }).then(function (data){
                    console.log(data);
                    toastr["success"]("Successful: Added Pet to Visit");
                });

            })
        });
    </script>
@endsection