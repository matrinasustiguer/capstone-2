@extends('layouts.template')
@section('title', "Prester's Friends")
@section('content')

@if(Session::has("message"))
    <h4 class="text-danger text-center">{{Session::get('message')}}</h4>
@endif
    <div class="px-4 py-2">
        <div class="row ml-1">
            <div class="clear-both col-lg-9">
                <h2 class="text-center pt-3">Meet Our Friends</h2>
                <div class="text-center pb-3 m-1 font-italic">Note: Before proceeding with the adoption process, we require at least 3 visits to the same pet. This helps ensure compatibility and safety of both parties as we want the best for both of you.</div>
                @auth
                    @if(Auth::user()->role_id === 2 | Auth::user()->role_id === 3)
                        <!-- Button trigger modal add-->
                        <div class="text-center p-2 clear-both">
                            <button type="button" class="btn-lg btn-outline-warning" data-toggle="modal" data-target="#exampleModal">Add Pet</button>
                        </div>
                        <!-- Modal Add-->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Pet Form</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                            <form action="/add-pet-form" method="POST" enctype="multipart/form-data" class="alpha-form" data-remote="true">
                                            @csrf
                                            @include('layouts.errors')
                                                <div class="form-group">
                                                    <label for="imgPath">Pet Image:</label>
                                                    <input type="file" name="imgPath" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name">Pet Name:</label>
                                                    <input type="text" name="name" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="age">Pet Age:</label>
                                                    <input type="number" name="age" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="type_id">Type:</label>
                                                    <select name="type_id" class="form-control">
                                                        @foreach($types as $type)
                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="description">Pet Description:</label>
                                                    <textarea name="description" class="form-control"></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-warning">Add Pet</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    @endif
                @endauth
                
                <ul class="nav nav-pills mt-5 bg-info downsh">
                    <li class="nav-item upw">
                        <a href="/Meet-Our-Friends" class="bg-info text-warning rounded nav-link flex-fill text-center mr-1 upsh">All</a>
                    </li>
                    @foreach($types as $type)
                    <li class="nav-item upw">
                        <a href="/Meet-Our-Friends?type_id={{$type->id}}" class="bg-info text-warning rounded nav-link flex-fill text-center mr-1 upsh">{{$type->name}}</a>
                    </li>
                    @endforeach
                    <li class="nav-item dropdown ml-auto mr-5 upw">
                        <a class="nav-link dropdown-toggle bg-white text-warning rounded-circle font-weight-bold" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Sort</a>
                        <div class="dropdown-menu bg-gray">
                            <a class="dropdown-item" href="/Meet-Our-Friends?sort_asc=asc">Youngest->Oldest</a>
                            <a class="dropdown-item" href="/Meet-Our-Friends?sort_desc=desc">Oldest->Youngest</a>
                        </div>
                    </li>
                </ul>
                <div class="row mt-1">      
                    @foreach($pets as $pet)
                    <div class="col-sm-6 col-lg-4 py-1">
                        <div class="card m-1 rounded-circle">
                            <img class="card-img-top rounded-circle img-thumbnail" src="{{$pet->imgPath}}">
                            <div class="card-body text-center">
                                <h3 class="card-title">{{$pet->name}}</h3>
                                <p class="card-text">Age: {{$pet->age}} yrs. old</p>
                                <p class="card-text">Description: {{$pet->description}}</p>
                                <p class="card-text">Type: {{$pet->type->name}}</p>
                            </div>
                            
                            @auth
                                @if(Auth::user()->role_id === 2 | Auth::user()->role_id === 3)

                            <div class="card-footer d-flex justify-content-center">
                                <form action="/delete-pet" method="POST">
                                @csrf
                                @method('DELETE')
                                    <input type="hidden" name="pet_id" value="{{$pet->id}}">
                                    <button type="submit" class="btn btn-danger mx-1">DELETE</button>
                                </form>
                                    <a href="/update-pet/{{$pet->id}}" class="btn btn-warning">Update</a>
                            </div>

                                @else
                                
                            <div class="card-footer d-flex justify-content-center">
                                <div class="input-group">
                                    <input type="hidden" name="pet_name" class="form-control" data-id="{{$pet->id}}" value="{{$pet->name}}">
                                    <div class="input-group-append m-auto">
                                        <button class="btn btn-primary rounded-circle addToBookBtn">Visit Me</button>
                                    </div>
                                </div>
                            </div>    
                                @endif
                            @else
                            <div class="card-footer d-flex justify-content-center">
                                <a class="btn btn-primary rounded-circle" href="/login">Visit Me</a>
                            </div>
                            @endauth    
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card bg-info rounded my-2 about-us">
                    <div class="card-body">
                        <img src="https://res.cloudinary.com/djo1ucwnk/image/upload/v1598419361/efpazr1kjtkkdvhh1ffo.png" alt="" height="80%" width="80%" class="d-block m-auto p-2 text-center align-items-center">
                        <h3 class="card-title">About Us</h3>
                        <p class="card-text text-warning">Prester's Pet Rescue and Shelter Society</p>
                        <p class="card-text">We are dedicated to rescuing animals and offering them shelter if not their next home. We now accept adoption applications from prospective parents interested. </p>
                        <p class="card-text">Here is our general adoption process:</p>
                        <p class="card-text">First: Let us get to know you. Login. (Not yet registered? Register.)
                        <p class="card-text">Second: On your right, you will see our pet friends who are up for adoption. Click Visit Me to schedule meeting with them. You can schedule more than one pet per visit.</p>
                        <p class="card-text">Third: After three successful visits, we will conduct a short interview and proceed with the paperworks. We have complete adoption papers and medical records (that's why we do have a small ADOPTION FEE). We keep them up to date. We love our pet friends!</p>
                        <p class="card-text">Fourth: You may bring your lovely pet home after the signing of papers.</p>
                        <p class="card-text">Fifth: We will keep in touch with you and visit our friend in your house after 2 days. We reserve the right to visit our pet friends at least twice a year with your permission in accordance with our policy. Again, we love our pet friends and it is in our intention that both of you are compatible and happy!</p>
                        <p class="card-text pt-1 mt-5 mx-auto align-items-center font-weight-bold">Contact Details:</p>
                        <p class="card-text mx-auto">
                            Call us: +02 7550 2X8X<br>
                            Phone Number: +63 906 701 4X1X<br>
                            Email: frontdesk@presters.com
                            Find us near SM Bicutan, Dona Soledad Avenue, Paranaque, 1709 Metro Manila
                        </p>
                        <div class="d-block m-auto p-2 text-center align-items-center">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3862.9609282421916!2d121.04195991459638!3d14.486932489874944!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397cf051470efb5%3A0xc5698f639aacfe68!2sSM%20City%20Bicutan!5e0!3m2!1sen!2sph!4v1591557320236!5m2!1sen!2sph" width="90%" height="80%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        const addToBookBtns = document.querySelectorAll('.addToBookBtn');
        addToBookBtns.forEach( function (addToBookBtn){
            addToBookBtn.addEventListener('click', function(e){
            
                const pet_name = e.target.parentElement.previousElementSibling.value;
                const pet_id = e.target.parentElement.previousElementSibling.getAttribute('data-id');

                let data = new FormData;
                data.append("_token", "{{ csrf_token() }}");
                data.append("pet_name", pet_name);

                fetch("/add-to-book/" + pet_id, {
                    method: "POST",
                    body: data //we need the data from the form
                }).then(function (response){
                    return response.text();
                }).then(function (data){
                    console.log(data);
                    toastr["success"]("Successful: Added Pet to Visit");
                });

            })
        });
    </script>
@endsection