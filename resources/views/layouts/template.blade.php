<!DOCTYPE html>
<html>
<head>

<meta property="og:image" content="https://res.cloudinary.com/djo1ucwnk/image/upload/v1598419361/efpazr1kjtkkdvhh1ffo.png" />
<meta property="og:description" content="We are dedicated to rescuing animals and offering them shelter if not their next home. We are accepting adoption applications. Book a visit!" />
<meta property="og:url" content="http://safe-dusk-16845.herokuapp.com/" />
<meta property="og:title" content="Prester's Pet Rescue and Shelter Society" />

	<title>@yield('title')</title>

	<link rel="stylesheet" href="https://bootswatch.com/4/litera/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">

	<script src="http://code.jquery.com/jquery-3.5.1.min.js" defer></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" defer></script>
	<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
	
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-info navsh">
	<a class="navbar-brand font-weight-bolder text-warning font-italic" href="/Meet-Our-Friends">
		<img src="https://res.cloudinary.com/djo1ucwnk/image/upload/v1598419361/efpazr1kjtkkdvhh1ffo.png" alt="" class="prester-art"> Prester's
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	
	<div class="collapse navbar-collapse" id="navbarColor01">
		<ul class="navbar-nav mr-auto">
		@auth
			@if(Auth::user()->role_id === 3)
			<li class="nav-item">
				<!-- <a class="nav-link" href="">Parent Applications</a> -->
			</li>
			<li class="nav-item">
				<!-- <a class="nav-link" href="">Pet Status</a> -->
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/all-visit-bookings">All Bookings</a>
			</li>
			@elseif(Auth::user()->role_id === 2)
			<li class="nav-item">
				<!-- <a class="nav-link" href="">Pet Status</a> -->
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/all-visit-bookings">All Bookings</a>
			</li>
			@else
			<li class="nav-item">
				<a class="nav-link" href="/my-visits">My Bookings</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/book">Book-My-Visit</a>
			</li>
			
			@endif	
		@endauth

		</ul>
		<ul class="navbar-nav ml-auto my-1">
		@auth
			
			<li class="nav-item dropdown">
				<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
					Hi {{ Auth::user()->name }}! <span class="caret"></span>
				</a>

				<div class="dropdown-menu dropdown-menu-right bg-info" aria-labelledby="navbarDropdown">
					
					<a class="dropdown-item" href="/my-profile">Profile</a>
					
					<form action="/logout" method="POST">
						@csrf
						<button class="dropdown-item">Logout</button>
					</form>	
					
				</div>
			</li>


			
		@else
			<li class="nav-item m-1">
				<a class="nav-link btn btn-outline-warning text-warning" href="/login">Login</a>
			</li>
			<li class="nav-item m-1">
				<a class="nav-link btn btn-outline-warning text-warning" href="/register">Register</a>
			</li>
		@endauth
		</ul>
	</div>
	</nav>

	@yield("content")

	<footer class="bg-info text-warning text-center mt-5 p-3">
	<img src="https://res.cloudinary.com/djo1ucwnk/image/upload/v1598419361/efpazr1kjtkkdvhh1ffo.png" alt="" class="prester-big d-block m-auto p-2 text-center align-items-center">
		All assets, logos, and trademarks used and/or referenced on this site are the assets, trademarks, and logos of their respective owners.
		<span class="d-block"><span class="font-weight-bold">Prester's Pet Rescue and Shelter Society</span>, Copyright@2020</span> 
	</footer>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>