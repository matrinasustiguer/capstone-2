<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="og:image" content="https://res.cloudinary.com/djo1ucwnk/image/upload/v1598419361/efpazr1kjtkkdvhh1ffo.png" />
	<meta property="og:description" content="We are dedicated to rescuing animals and offering them shelter if not their next home. We are accepting adoption applications. Book a visit!" />
	<meta property="og:url" content="http://safe-dusk-16845.herokuapp.com/" />
	<meta property="og:title" content="Prester's Pet Rescue and Shelter Society" />

    <title>Prester's</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="https://bootswatch.com/4/litera/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/welcome.css') }}">
</head>
<body>
    <div id="app">
        <a class="upper-left prester-logo text-warning" href="{{ url('/') }}">
            <img src="https://res.cloudinary.com/djo1ucwnk/image/upload/v1598419361/efpazr1kjtkkdvhh1ffo.png" alt="" height="55em"> Prester's
        </a>  
        <div class="upper-right">
            @guest
                <a href="{{ route('login') }}" class="btn btn-outline-warning">Login</a>
            
                @if (Route::has('register'))
                <a href="{{ route('register') }}" class="btn btn-outline-warning">Register</a>
                @endif
            
            @else
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <a class="btn btn-outline-warning" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            
            @endguest
            </div>
        <div class="py-5"></div>           
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
