@extends('layouts.template')
@section('title', 'My Profile')
@section('content')
    @auth
    @if(isset($profile)) 
        <div class="container">
                <div class="d-flex justify-content-center align-items-center flex-column col-lg-6 offset-lg-3">
                    <h1 class="m-2 p-2 font-italic">My Profile</h1>
                    <img src="{{$profile->profImg}}" alt="your profile picture" class="border m-5 rounded-circle img-thumbnail" height="200px" width="200px">
                              
                    <div class="card bg-info rounded-circle text-center">
                        <div class="card-body">
                            <h4 class="card-title">Name: {{Auth::user()->name}}</h4><br>
                            <p class="card-text">Email: {{Auth::user()->email}}</p>
                            <p class="card-text">Contact No.: {{$profile->contact}}</p>
                            <p class="card-text">Address: {{$profile->address}}</p><br>
                            <p class="card-text">About:</p>
                            <p class="card-text">{{$profile->about}}</p>
                        </div>
                        <div class="card-footer d-flex justify-content-center">
                            <a href="/update-profile/{{$profile->id}}" class="btn btn-outline-warning my-1">Update Profile</a>
                            <form action="/delete-profile" method="POST">
                                @csrf
                                @method('DELETE')
                                    <input type="hidden" name="profile_id" value="{{$profile->id}}">
                                    <button type="submit" class="btn btn-outline-danger m-1">DELETE</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        @else        
        <div class="d-flex justify-content-center align-items-center flex-column vh-100 text-warning">
            <h3 class="p-3 alpha-form rounded">Nothing to Show. Please Set Up Your Profile.</h3>
            <!-- Button trigger modal add-->
            <div class="text-center p-2 clear-both">
                <button type="button" class="btn-lg btn-info" data-toggle="modal" data-target="#profileModal">Set Up Profile</button>
            </div>
        </div>
        <!-- Modal Add-->
        <div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="profileModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="profileModalLabel">Add Profile Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <form action="/add-profile" method="POST" enctype="multipart/form-data" class="alpha-form" data-remote="true">
                                @csrf
                                @include('layouts.errors')
                                    <div class="form-group">
                                        <label for="profImg">Profile Picture:</label>
                                        <input type="file" name="profImg" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Contact Number:</label>
                                        <input type="text" name="contact" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Address:</label>
                                        <input type="text" name="address" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="about">About Me:</label>
                                        <textarea name="about" class="form-control" placeholder="Say something about yourself.."></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-warning">Save Profile</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
            </div>

    @endif
    @endauth
@endsection