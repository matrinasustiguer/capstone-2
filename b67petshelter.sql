-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 25, 2020 at 08:18 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `b67petshelter`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_08_10_125104_create_roles_table', 2),
(5, '2020_08_10_125229_create_types_table', 2),
(6, '2020_08_10_125428_create_statuses_table', 2),
(7, '2020_08_10_125502_create_standings_table', 2),
(8, '2020_08_10_125555_add_roles_to_users', 2),
(9, '2020_08_10_125702_create_pets_table', 2),
(10, '2020_08_10_125937_add_standings_to_users', 2),
(11, '2020_08_10_130202_create_visits_table', 2),
(12, '2020_08_10_130713_create_pet_visit_table', 2),
(18, '2020_08_25_084146_create_profiles_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pets`
--

CREATE TABLE `pets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imgPath` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `status_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pets`
--

INSERT INTO `pets` (`id`, `name`, `description`, `imgPath`, `type_id`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Mimo', 'Gentle but Playful. Black/White. Male. 2 yrs old.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598255235/ithkguegtvwgtoxva6sa.jpg', 1, 1, '2020-08-23 23:47:16', '2020-08-24 05:39:55'),
(2, 'Maddy', 'Energetic and Smart. Black/White. Female. 1 yr old.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598255771/ox6chbilyuzssm0rfrmu.jpg', 2, 1, '2020-08-23 23:56:12', '2020-08-23 23:56:12'),
(3, 'Tim', 'Loves Walks and Cuddles. Black/Brown/White. Male. 3.5 yrs old.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598255943/yrpugqbd4urnqtpb8yg9.jpg', 2, 1, '2020-08-23 23:59:04', '2020-08-23 23:59:04'),
(4, 'Rafa', 'Loves Treats and Boxes. Gray. Female. 1.5 yrs old.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598256032/v03k2rchfcgpx8bvqqed.jpg', 1, 1, '2020-08-24 00:00:32', '2020-08-24 00:00:32'),
(5, 'Nigel', 'Smart and Sweet. Orange. Male. 2.5 yrs old.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598256218/urbpbyrw8ktyr3owew6e.jpg', 1, 1, '2020-08-24 00:03:39', '2020-08-24 00:03:39'),
(6, 'Lyndon', 'Friendly and Playful. Black/White. Male. 2.5 yrs old.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598257105/fsbf8lcvtmgsltxrp82b.jpg', 2, 1, '2020-08-24 00:18:26', '2020-08-24 00:49:11'),
(7, 'Karl', 'Curious and Funny. White/Brown. Male. 1.5 yrs old.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598257731/otldfjrehs1znpppkv7d.jpg', 1, 1, '2020-08-24 00:28:51', '2020-08-24 00:50:54'),
(8, 'Rima', 'Gentle and Loves to Eat. Red Fawn. Female. 3.5 yrs old.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598258509/fexvxlnzanffe5izeera.jpg', 2, 1, '2020-08-24 00:41:49', '2020-08-24 00:41:49'),
(9, 'Dolly', 'Loves Sleeping and Grooming. Gray/White. Female. 3 yrs old.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598259515/djrwfk9rulk8ttrteux8.jpg', 1, 1, '2020-08-24 00:58:35', '2020-08-24 00:58:35'),
(10, 'Megan', 'Gentle and Curious. Brown/White. Female. 2 yrs old.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598259752/cwtuerd012xtakrqqcbj.jpg', 2, 1, '2020-08-24 01:02:32', '2020-08-24 01:02:32'),
(11, 'Cheetah', 'Likes Blankets.', 'images/1598356463.jpeg', 1, 1, '2020-08-24 07:13:21', '2020-08-25 03:54:23');

-- --------------------------------------------------------

--
-- Table structure for table `pet_visit`
--

CREATE TABLE `pet_visit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pet_id` bigint(20) UNSIGNED NOT NULL,
  `visit_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profImg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `contact`, `address`, `about`, `profImg`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '09999999991', '123 Num Street, Admin City', 'I\'m the admin of this site. I love animals and the environment.', 'https://res.cloudinary.com/djo1ucwnk/image/upload/v1598356828/wcn06tlcyjbgdtyayysy.jpg', 1, '2020-08-25 04:00:28', '2020-08-25 04:51:51');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Customer', '2020-08-23 09:16:51', '2020-08-23 09:16:51'),
(2, 'Frontdesk', '2020-08-23 09:16:51', '2020-08-23 09:16:51'),
(3, 'Admin', '2020-08-23 09:16:51', '2020-08-23 09:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `standings`
--

CREATE TABLE `standings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `standings`
--

INSERT INTO `standings` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Applicant', '2020-08-23 09:16:51', '2020-08-23 09:16:51'),
(2, 'Approved', '2020-08-23 09:16:51', '2020-08-23 09:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Rescued', '2020-08-23 09:16:51', '2020-08-23 09:16:51'),
(2, 'For-Adoption', '2020-08-23 09:16:51', '2020-08-23 09:16:51'),
(3, 'Reserved', '2020-08-23 09:16:51', '2020-08-23 09:16:51'),
(4, 'Adopted', '2020-08-23 09:16:51', '2020-08-23 09:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Feline / Cats', '2020-08-23 21:56:38', '2020-08-23 21:56:38'),
(2, 'Canine / Dogs', '2020-08-23 21:56:48', '2020-08-23 21:56:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `standing_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`, `standing_id`) VALUES
(1, 'Admin Presters', 'admin@presters.com', NULL, '$2y$10$l4ocwxf4F6yFMxauEW03N.He2W.seAVHf2o.X1DSLotNp/xlm.Wxe', NULL, '2020-08-23 23:28:27', '2020-08-23 23:28:27', 3, 2),
(2, 'Customer One', 'customer@one.com', NULL, '$2y$10$GsWGmOVTwzth9BnX06xW3OHrcjGu5hh2dmmVe/mSYfU3tb6KyApOi', NULL, '2020-08-24 01:03:51', '2020-08-24 01:03:51', 1, 1),
(3, 'Frontdesk Presters', 'frontdesk@presters.com', NULL, '$2y$10$JEq7B6JF9qA8iAbQjK2c2Oy3ZLARsb90u0I5ggEyOG9G1c0nkeVC2', NULL, '2020-08-24 01:07:40', '2020-08-24 01:07:40', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `visits`
--

CREATE TABLE `visits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `standing_id` bigint(20) UNSIGNED NOT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pets_type_id_foreign` (`type_id`),
  ADD KEY `pets_status_id_foreign` (`status_id`);

--
-- Indexes for table `pet_visit`
--
ALTER TABLE `pet_visit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pet_visit_pet_id_foreign` (`pet_id`),
  ADD KEY `pet_visit_visit_id_foreign` (`visit_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `standings`
--
ALTER TABLE `standings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`),
  ADD KEY `users_standing_id_foreign` (`standing_id`);

--
-- Indexes for table `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visits_user_id_foreign` (`user_id`),
  ADD KEY `visits_standing_id_foreign` (`standing_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pets`
--
ALTER TABLE `pets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pet_visit`
--
ALTER TABLE `pet_visit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `standings`
--
ALTER TABLE `standings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `visits`
--
ALTER TABLE `visits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pets`
--
ALTER TABLE `pets`
  ADD CONSTRAINT `pets_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pets_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `pet_visit`
--
ALTER TABLE `pet_visit`
  ADD CONSTRAINT `pet_visit_pet_id_foreign` FOREIGN KEY (`pet_id`) REFERENCES `pets` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pet_visit_visit_id_foreign` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `users_standing_id_foreign` FOREIGN KEY (`standing_id`) REFERENCES `standings` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `visits`
--
ALTER TABLE `visits`
  ADD CONSTRAINT `visits_standing_id_foreign` FOREIGN KEY (`standing_id`) REFERENCES `standings` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `visits_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
