<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('statuses')->delete();

        \DB::table('statuses')->insert(array(
            0 =>
            array (
                'id' => 1,
                'name' => 'Rescued',
                'created_at' => now(),
                'updated_at' => now()
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'For-Adoption',
                'created_at' => now(),
                'updated_at' => now()
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Reserved',
                'created_at' => now(),
                'updated_at' => now()
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Adopted',
                'created_at' => now(),
                'updated_at' => now()
            )
        ));
    }
}
