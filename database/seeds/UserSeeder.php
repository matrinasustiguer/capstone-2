<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();

        \DB::table('users')->insert(array(
            0 =>
            array (
                'id' => 1,
                'name' => 'Admin Presters',
                'email' => 'admin@presters.com',
                'password' => Hash::make('adminpass'),
                'role_id' => 3,
                'standing_id' => 2
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Frontdesk Presters',
                'email' => 'frontdesk@presters.com',
                'password' => Hash::make('frontdeskpass'),
                'role_id' => 2,
                'standing_id' => 2
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Customer One',
                'email' => 'customer@one.com',
                'password' => Hash::make('onepass'),
                'role_id' => 1,
                'standing_id' => 1
            )
        ));
    }
}
