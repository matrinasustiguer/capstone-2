<?php

use Illuminate\Database\Seeder;

class StandingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('standings')->delete();

        \DB::table('standings')->insert(array(
            0 =>
            array (
                'id' => 1,
                'name' => 'Applicant',
                'created_at' => now(),
                'updated_at' => now()
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Approved',
                'created_at' => now(),
                'updated_at' => now()
            )
        ));
    }
}
